import React from 'react';

import { CardTodo, Title, IsDone } from './StyledComponents';

// Data must have these properties
interface TodoProps {
  title: string;
  done: boolean;
}

export const Todo: React.FC<TodoProps> = ({ title, done }): JSX.Element => {
  return (
    <CardTodo>
      <Title>{title}</Title>
      <IsDone>{done ? 'Completed' : 'Not Completed'}</IsDone>
    </CardTodo>
  );
};
