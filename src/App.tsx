import axios, { AxiosResponse } from 'axios';
import { Todo } from './components/Todo';
import { Component } from 'react';

import { AppElement, Todos, Loading } from './components/StyledComponents';

interface Data {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}

class App extends Component {
  state: { leftData: Data[]; rightData: Data[] } = {
    leftData: [],
    rightData: [],
  };

  componentDidMount = (): void => {
    axios
      .get('https://jsonplaceholder.typicode.com/todos')
      .then((res: AxiosResponse): void => {
        let newDataLeft = res.data
          .filter((d: Data): boolean => d.completed === true)
          .slice(0, 7);
        let newDataRight = res.data
          .filter((d: Data): boolean => d.completed === false)
          .slice(0, 7);
        this.setState({
          leftData: [...newDataLeft],
          rightData: [...newDataRight],
        });
      })
      .catch((err: Error): void => {
        console.log(err);
      });
  };

  render() {
    return (
      <AppElement>
        <Todos>
          {this.state.leftData ? (
            <>
              <div id="left-todos">
                {this.state.leftData.map(
                  (d: Data): JSX.Element => {
                    return (
                      <Todo title={d.title} done={d.completed} key={d.id} />
                    );
                  }
                )}
              </div>
              <div id="right-todos">
                {this.state.rightData.map(
                  (d: Data): JSX.Element => {
                    return (
                      <Todo title={d.title} done={d.completed} key={d.id} />
                    );
                  }
                )}
              </div>
            </>
          ) : (
            <Loading>Loading ...</Loading>
          )}
        </Todos>
      </AppElement>
    );
  }
}

export default App;
