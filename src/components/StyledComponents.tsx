import styled from 'styled-components';

// App
export const AppElement = styled.div`
  height: 100%;
  width: 100%;
  background-color: #f1f3f6;
`;

export const Todos = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 30px 0 30px;
`;

export const Loading = styled.div`
  font-size: 20px;
  font-weight: 600;
  color: #bad86f;
`;

// Todo
export const CardTodo = styled.div`
  width: 800px;
  height: 100px;
  padding: 8px 14px 8px 14px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 8px;
  background-color: #f1f3f6;
  color: #0e3d67;
  margin-top: 10px;
  box-shadow: -1px -1px 12px 1px grey;
`;

export const Title = styled.div`
  font-size: 22px;
  font-weight: 600;
`;

export const IsDone = styled.div`
  background-color: #bad86f;
  width: 150px;
  text-align: center;
  padding: 6px 3px 6px; 3px;
  border-radius: 5px;
  font-size: 18px;
  font-weight: 500;
  transform: translate(0, 80%);
`;
